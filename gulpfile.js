'use strict';

/* Specifying gulp dependencies */
var gulp        = require('gulp'),

    /* used for reloading changes into the browser on file save to increase decelopment speed */
    browserSync = require('browser-sync').create(),

    /* just simplifying the reload browser method */
    reload      = browserSync.reload,

    /* used for compressing css files */
    cssmin      = require('gulp-cssmin'),

    /* used for taking the content from one or several files and putting that content into one file */
    concat      = require('gulp-concat'),

    /* used for compressing javascript, we can use the words minifying and compresssing interchangeably */
    uglify      = require('gulp-uglify'),

    /* rimraf is used for deleting files, pretty cool huh! */
    rimraf      = require('rimraf'),

    /* used for compressing html */
    htmlmin     = require('gulp-htmlmin'),

    /* used for renaming files */
    rename      = require('gulp-rename'),

    /* used for compiling scss files into css files */
    sass        = require('gulp-sass');

var filePathsDev = {
    cssFiles: "server/static/css/*.css",
    jsFiles: "server/static/js/*.js",
    htmlFiles: "server/templates/*.html",
    scssFiles: "server/static/scss/*.scss"
};

var folderLocationsDev = {
    css: "server/static/css"
};

var filePathsProd = {

};

gulp.task('default', [], () => {
    log("[+] gulp is working. Stay calm. Keep writing code");
});

/* Run gulp serve:dev to work in development mode */
gulp.task("serve:dev", () => {

    browserSync.init({
        proxy: "localhost:8080"
    });

    gulp.watch(filePathsDev.cssFiles).on('change', reload);
    gulp.watch(filePathsDev.jsFiles).on('change', reload);
    gulp.watch(filePathsDev.htmlFiles).on('change', reload);
    gulp.watch(filePathsDev.scssFiles, ['compile:sass']);
    // gulp.watch(filePathsDev.scssFiles).on('change', reload);

});

gulp.task('compile:sass', [], () => {
    return gulp.src(filePathsDev.scssFiles)
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(folderLocationsDev.css));
});

let log = (msg) => {
    console.log(msg);
}