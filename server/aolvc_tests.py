from main import app
import os
import unittest
import tempfile


class AolvcTestCase(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()
        self.app.testing = True

    def tearDown(self):
        pass

    def test_create_event(self):
        result = self.app.get('/create_online_event', data='{"center":{"name":"Art of Living Los Angeles","phone":"","email":"losangeles@us.artofliving.org","address":""},"location":{"latitude":"0.000000","longitude":"0.000000"},"address":{"country":"","state":"","zipcode":"","city":"","street_address_1":"","street_address_2":""},"event_end":{"timezone":"America/Los_Angeles","utc":"2018-02-28T10:00:00","local":"2018-02-28T02:00:00"},"event_start":{"timezone":"America/Los_Angeles","utc":"2018-02-28T09:00:00","local":"2018-02-28T01:00:00"},"template_id":"5a38bd92cb3860207264d2b8","additional_details":"","online_session_url":"www.sdf.mnb","speaker_name":"john","event_capacity":"23","event_description":"","event_series_name":"00609ba1a3d266bc4cf6502efccecf8e","event_name":"Mind and Meditation","event_id":"a163A000009qUR9QAM","organizers":[{"phone":"6178589140","email":"vinodt@artofliving.org","name":"vinod thiagarajan"}],"private_event":false,"event_type":"online","event_status":"active"}')
        print result.data


if __name__ == '__main__':
    unittest.main()