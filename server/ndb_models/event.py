from google.appengine.ext import ndb
from participant import Participant
from database_helper import create_random_link, get_time_for_datastore
from twilio_channel import *
from helper_functions import *
import json
import time


class Event(ndb.Expando):
    time_added = ndb.StringProperty()
    event_id = ndb.StringProperty()
    organizers = ndb.JsonProperty()
    start_time = ndb.JsonProperty()
    end_time = ndb.JsonProperty()
    event_json = ndb.JsonProperty()
    organizer_link = ndb.StringProperty()
    participants = ndb.StructuredProperty(Participant, repeated=True)
    title = ndb.StringProperty()
    channel = ndb.StructuredProperty(TwilioChannel)
    expired = ndb.BooleanProperty()

    @classmethod
    def store_event(cls, event_data):
        # extract the data from the dictionary and format it for storage
        event_json = json.dumps(event_data)
        event_id = event_data["event_id"]
        organizers = json.dumps(event_data["organizers"])
        start_time = json.dumps(event_data["event_start"])
        end_time = json.dumps(event_data["event_end"])
        title = event_data["event_name"]

        # create the unique link for the organizer/s
        organizer_link = cls.create_unique_organizer_link()

        # finally, create the new event db entity and write it to the db asynchronously
        new_event = cls(
            time_added=get_time_for_datastore(),
            event_id=event_id,
            organizers=organizers,
            start_time=start_time,
            end_time=end_time,
            event_json=event_json,
            organizer_link=organizer_link,
            participants=[],
            title=title,
            channel=None,
            expired=False
        )
        new_event.put()

        # return the organizer link because we will be sending that in the response
        return organizer_link

    @classmethod
    def create_unique_organizer_link(cls):
        random_link = create_random_link()
        while cls.link_exists(random_link):
            random_link = create_random_link()
        return random_link

    @classmethod
    def link_exists(cls, random_link):
        return cls.query(cls.organizer_link == random_link).get() is not None

    @classmethod
    def event_exists(cls, event_id):
        return cls.query(cls.event_id == event_id).get() is not None

    @classmethod
    def by_link(cls, link):
        return cls.query(cls.organizer_link == link).get()

    def add_participant(self, username, email, phone):
        self.participants.append(
            Participant(
                username=username,
                email=email,
                phone=phone
            )
        )
        self.put()

    def create_twilio_channel(self, unique_name, sid):
        self.channel = TwilioChannel(
            time_created=get_time_for_datastore(),
            friendly_name=self.title,
            unique_name=unique_name,
            sid=sid
        )
        self.put()

    def set_expired(self, boolean):
        self.expired = boolean
        self.put()

    def get_organizers(self):
        organizer_names = []
        organizers = json.loads(self.organizers)
        for organizer in organizers:
            organizer_names.append(capitalize_name(organizer.get("name")))
        return organizer_names

    def has_organizer(self, twilio_identity):
        name = twilio_identity.split(":")[0]
        organizers = self.get_organizers()
        for organizer in organizers:
            if organizer.lower() == name.lower():
                return True
        return False


"""
{
    "center" : {
        "name" : "Art of Living Los Angeles",
        "phone" : "",
        "email" : "losangeles@us.artofliving.org",
        "address" : ""
    },
    "location" : {
        "latitude" : "0.000000",
        "longitude" : "0.000000"
    },
    "address" : {
        "country" : "",
        "state" : "",
        "zipcode" : "",
        "city" : "",
        "street_address_1" : "",
        "street_address_2" : ""
    },
    "event_end" : {
        "timezone" : "America/Los_Angeles",
        "utc" : "2018-02-28T10:00:00",
        "local" : "2018-02-28T02:00:00"
    },
    "event_start" : {
        "timezone" : "America/Los_Angeles",
        "utc" : "2018-02-28T09:00:00",
        "local" : "2018-02-28T01:00:00"
    },
    "template_id" : 5a38bd92cb3860207264d2b8,
    "additional_details" : "",
    "online_session_url" : "www.sdf.mnb",
    "speaker_name" : "john",
    "event_capacity" : "23",
    "event_description" : "",
    "event_series_name" : "00609ba1a3d266bc4cf6502efccecf8e",
    "event_name" : "Mind and Meditation",
    "event_id" : "a163A000009qUR9QAM",
    "organizers" : [ 
        {
            "phone" : "6178589140",
            "email" : "vinodt@artofliving.org",
            "name" : "vinod thiagarajan"
        }
    ],
    "private_event" : false,
    "event_type" : "online",
    "event_status" : "active"
}
"""

