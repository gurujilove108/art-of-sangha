from google.appengine.ext import ndb
from database_helper import create_random_link, get_time_for_datastore

class TwilioChannel(ndb.Expando):
    time_created = ndb.StringProperty()
    # unique channel name for twilio channel
    friendly_name = ndb.StringProperty()
    unique_name = ndb.StringProperty()
    # channel sid
    sid = ndb.StringProperty()

    @classmethod
    def channel_exists(cls, channel_name):
        return cls.query(cls.unique_name == channel_name).get() is not None



