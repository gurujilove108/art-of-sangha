from google.appengine.ext import ndb


class Participant(ndb.Expando):
    email = ndb.StringProperty()
    username = ndb.StringProperty()
    phone_number = ndb.StringProperty()
    is_active = ndb.BooleanProperty()

