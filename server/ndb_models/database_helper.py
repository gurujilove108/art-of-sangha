import string
import random
import time

base_string = string.letters + string.digits + string.letters + string.letters


# generate a string of random letters and numbers to use for event links
def create_random_link():
    random_link_length = random.randint(10, 30)
    random_link_characters = []
    for i in range(random_link_length):
        random_link_characters.append(base_string[random.randint(0, len(base_string) - 1)])
    return "".join(random_link_characters)


# get the current timestamp and format it for google cloud datastore
def get_time_for_datastore():
    return str(int(time.time()))