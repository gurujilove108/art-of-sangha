from helper_functions import log


class MiddleWare(object):
    """
    simple middleware for our app
    """
    def __init__(self, app):
        self.app = app

    def __call__(self, environ, start_response):
        return self.app(environ, start_response)