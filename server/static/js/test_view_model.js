(($, window) => {
    // noinspection JSUnusedGlobalSymbols
    let indexViewModel = {
        currentEvent: ko.observable(),
        events: ko.observableArray(),
        showEvent: (data) => {
            indexViewModel.currentEvent(data);
        },
        deleteAllEvents: () => {
            $.get('/delete_events', (data) => {});
        },
        fetchEventJson: () => {
            $.get('/event_json', (eventJson) => {
                eventJson.forEach(event => indexViewModel.events.push(event));

            });
        }
    };

    indexViewModel.fetchEventJson();
    ko.applyBindings(indexViewModel);
})(jQuery, window);