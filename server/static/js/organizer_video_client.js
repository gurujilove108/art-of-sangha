var VideoClient = function() {
    this.organizerVideoContainerId = "video-container";
};

VideoClient.prototype.detachTracks = function(tracks) {
    tracks.forEach(function(track) {
        track.detach().forEach(function(detachedElement) {
            detachedElement.remove();
        });
    });
};

VideoClient.prototype.detachParticipantTracks = function(participant) {
    let tracks = Array.from(participant.tracks.values());
    this.detachTracks(tracks);
};

VideoClient.prototype.handleParticipantConnected = function(participant) {
    console.log("participantConnected", participant);
    // this.addParticipantTracksToView(participant);
};

VideoClient.prototype.handleParticipantDisconnected = function(participant) {
    console.log("participant has disconnected", participant);
    // this.detachParticipantTracks(participant);
};

VideoClient.prototype.addTrackToView = function(track, identity) {
    console.log("adding track to view");
    // let selector = "[video-identity='%s']".replace("%s", identity);
    // let element = document.querySelector(selector);
    // element.appendChild(track.attach());
};

VideoClient.prototype.handleRoomJoined = function(room) {
    console.log("joined room ", room);
    let self = this;
    console.log(self);
    room.on('participantConnected', self.handleParticipantConnected);
    room.on('participantDisconnected', self.handleParticipantDisconnected);

    // When a Participant removes a Track, detach it from the DOM.
    room.on('trackRemoved', function(track, participant) {
        eatSnacks(extractName(participant.identity) + " removed track: " + track.kind);
        self.detachTracks([track]);
    });

    room.on('disconnected', () => {
        self.detachParticipantTracks(room.localParticipant);
        room.participants.forEach(self.detachParticipantTracks);
    });

    window.room = room;

    room.localParticipant.tracks.forEach(track => {
        jQuery("#user-icon-no-video").addClass("hide");
        document.getElementById(self.organizerVideoContainerId).appendChild(track.attach());
    });

    room.on("trackAdded", (track, participant) => {
        console.log(participant, " has added track", track);
        self.addTrackToView(track, participant.identity);
    });

    room.participants.forEach(participant => {
        // self.addParticipantTracksToView(participant);
    });
};

VideoClient.prototype.handleRoomError = function(error) {
    console.log("error joining video room");
    console.log(error);
};

VideoClient.prototype.startVideo = function(json_data) {
    console.log("starting video");
    let self = this;
    console.log(self);
    console.log(json_data);
    Twilio.Video.connect(json_data.token).then(function(room) {
        self.handleRoomJoined(room);
    }, function(error) {
        self.handleRoomError(error);
    });
};

VideoClient.prototype.start = function() {
    // $.get(location.origin + location.pathname + "/video_access_token", startVideo);
    let self = this;
    console.log(self);
    $.get(location.origin + location.pathname + "/start_video", function(data) {
        self.startVideo(data);
    });
};

VideoClient.prototype.setStartCallOnclick = function() {
    let self = this;
    let startCallButton = jQuery("#start-stop-call-btn");
    startCallButton.click(function() {
        if (!startCallButton.hasClass("session-active")) {
            self.start();
            startCallButton.addClass("session-active");
        } else if (startCallButton.hasClass("session-active")) {
            $.get(location.origin + location.pathname + "/end_session", function(data) {
                console.log("session has ended");
            });
        }
    });
};


VideoClient.prototype.setCameraButtonOnclick = function() {
    jQuery("#top-controls-camera").click(() => {

    });
};


