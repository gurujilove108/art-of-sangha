let setOnResize = () => {

    jQuery(window).resize(function() {
        let rowContainerWidth = jQuery("#signin-row").width();
        jQuery('.login-btn').css({"width": (rowContainerWidth * 0.2).toString()});
    });
};

setOnResize();
jQuery(window).resize();