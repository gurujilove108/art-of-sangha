var ChatClient = function() {
    this.currentChannelSid = null;
};

ChatClient.prototype.start = function() {
    let self = this;
    $.get('/chat_access_token', (json_data) => {
        self.initializeChatClient(json_data);
    });
};

ChatClient.prototype.initializeChatClient = function (json_data) {
    console.log(json_data);
    let self = this;
    if (json_data.error)
        alert("error retrieving chat access token");
    else if (json_data.token) {
        Twilio.Chat.Client.create(json_data.token).then(client => {
            self.setupChatClient(client);
        }).catch(this.handleChatClientCreationError);
    }
};

ChatClient.prototype.setupChatClient = function(client) {
    let self = this;
    console.log("chat client success");
    console.log(client);
    this.chatClient = client;
    this.getAndSetChannelSid();
    client.on("channelJoined", (channel) => {
        if (channel.sid === self.currentChannelSid)
            self.handleChannelJoined(channel);
        else if (self.currentChannelSid == null) {
            ((possibleChannel) => {
                $.get(location.origin + location.pathname + "/channel_sid", (json_data) => {
                 if (json_data.sid === possibleChannel.sid)
                     self.handleChannelJoined(possibleChannel);
                });
            })(channel);
        }
    });
    client.on("tokenExpired", self.refreshAccessToken);
};

ChatClient.prototype.handleChatClientCreationError = function(error) {
    console.log("error");
    console.log(error);
};

ChatClient.prototype.refreshAccessToken = function(twilioChatClient) {
    $.get("/chat_access_token", (json_data) => {
        twilioChatClient.updateToken(json_data.token);
    });
};

ChatClient.prototype.getAndSetChannelSid = function() {
    let self = this;
    $.get(location.origin + location.pathname + "/channel_sid", (json_data) => {
        if (json_data.sid)
            self.currentChannelSid = json_data.sid;
    });
};

ChatClient.prototype.handleChannelJoined = function(channel) {
    let self = this;
    eatSnacks("Joined channel: " + channel.friendlyName);
    /* get all members for the channel and pass the members to the handleMembers callback */
    channel.getMembers().then(self.handleMembers);
    /* get all messages then pass the messages to the handleMessages callback */
    channel.getMessages().then(self.handleMessages);
    channel.on("messageAdded", self.handleNewMessage);
    /* Listen for members joining this channel */
    channel.on('memberJoined', self.handleMemberJoined);
    /* Listen for members leaving a channel */
    channel.on('memberLeft', self.handleMemberLeft);
    /* not really doing anything with this yet, just putting it in for future use */
    channel.on('memberInfoUpdated', self.handleMemberInfoUpdated);
    channel.on('typingStarted', self.handleTypingStarted);
    channel.on('typingEnded', self.handleTypingEnded);
    this.setSendMessageBtnOnclick(channel);
    // setWindowOnBeforeUnload(channel);
};

ChatClient.prototype.handleTypingEnded = (member) => {
    eatSnacks(extractName(member.identity) + " has stopped typing");
};
ChatClient.prototype.handleTypingStarted = (member) => {
    eatSnacks(extractName(member.identity) + " has started typing");
};

ChatClient.prototype.handleMemberInfoUpdated = (member) => {
    eatSnacks(extractName(member.identity) + ' updated their info.');
    console.log(member);
};
ChatClient.prototype.handleMemberLeft = function(member) {
    eatSnacks(extractName(member.identity) + ' has left the channel.');
    let self = this;
    window.viewModel.numParticipants(window.viewModel.numParticipants() - 1);
    window.viewModel.removeMember(member.identity);
};

ChatClient.prototype.handleMemberJoined = function(member) {
    /* member just joined the channel so add member to our view model chatParticipants array, chatParticipants and memebers are same thing here */
    /* also increment the numParticipants observable */
    let self = this;
    window.viewModel.numParticipants(window.viewModel.numParticipants() + 1);
    eatSnacks(extractName(member.identity) + ' has joined the channel.');
    window.viewModel.chatParticipants.push(member);
};

ChatClient.prototype.handleNewMessage = function(message) {
    let self = this;
    window.viewModel.messages.push(message);
    scrollChatContainerIntoView();
};

ChatClient.prototype.setSendMessageBtnOnclick = function(channel) {
    let sendMessageBtn = $("#send-btn");
     let chatMessage = $("#send-message-input");
     sendMessageBtn.click(() => {
         let message = chatMessage.val().trim();
         console.log("sending message " + message);
         if (message.length > 0) {
             channel.sendMessage(message);
             chatMessage.val("");
         }
     });
     chatMessage.keyup((event) => {
        if (event.keyCode === 13) {
            sendMessageBtn.click();
        }
     });
};

ChatClient.prototype.handleMembers = function(members) {
    let self = this;
    /* iterate through each member and add the member object to knockout view model */
    members.forEach(member => {
        window.viewModel.chatParticipants.push(member)
    });
    /* update the number of chatParticipants */
    window.viewModel.numParticipants(members.length);
};

ChatClient.prototype.handleMessages = function(messages) {
    let self = this;
    messages.items.forEach(message => {
        window.viewModel.messages.push(message);
    });
};


ChatClient.prototype.setUserLeaveFunctions = function() {
    // jQuery(window).focus(() => {
    //
    // })
};

