let currentWidth  = () => {
    return jQuery(window).width();
};

let currentHeight = () => {
    return jQuery(window).height();
};

let isTabletOrMedium = () => {
    let cw = currentWidth();
    return cw >= 768 && cw < 1200;
};

