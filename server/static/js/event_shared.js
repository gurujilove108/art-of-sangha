/* launch the snackbar */
let eatSnacks = (message) => {
    let x = document.getElementById("snackbar");
    x.className = "show";
    jQuery(x).text(message);
    setTimeout(() => {x.className = x.className.replace("show", "");}, 2000);
};

/* on every chat message, we grab the last message and scroll it into view */
/* also do the same thing with the participants */
let scrollChatContainerIntoView = () => {
    let messageContainer = document.getElementById("chat-messages-column");
    let lastMessage = messageContainer.children[messageContainer.children.length-1];
    lastMessage.scrollIntoView();
};

let setChatToggle = () => {
    let chatToggleButton = jQuery("#chat-toggle-container");
    let videoColumn = jQuery("#video-column");
    jQuery(chatToggleButton).click(() => {
        if (videoColumn.hasClass("chat-open")) {
            videoColumn.removeClass("chat-open");
            videoColumn.removeClass("col-sm-8");
            videoColumn.addClass("col-sm-12");
            videoColumn.removeClass("col-lg-9");
            videoColumn.addClass("col-lg-12");
        }
        let chatToggleCloneContainer = document.createElement("div");
        jQuery(chatToggleCloneContainer).css({
            height: "100%",
            display: "flex",
            alignItems: "center",
            position: "fixed",
            right: "0",
            top: "-68px",
        });
        let chatToggleClone = jQuery("#chat-toggle-container").clone();
        jQuery(chatToggleCloneContainer).append(chatToggleClone);
        videoColumn.append(chatToggleCloneContainer);
        chatToggleClone.css({
           position: "fixed",
           left: "auto",
           right: "0",
        });
        jQuery(chatToggleClone.find("i")).removeClass("fa-angle-right");
        jQuery(chatToggleClone.find("i")).addClass("fa-angle-left");
        jQuery(chatToggleClone).click(() => {
           jQuery(chatToggleCloneContainer).remove();
           videoColumn.addClass("chat-open");
           videoColumn.removeClass("col-sm-12");
           videoColumn.addClass("col-sm-8");
           videoColumn.removeClass("col-lg-12");
           videoColumn.addClass("col-lg-9");
        });
    });
};

let showVideoContainerOptions = () => {
    jQuery("#star-icon").removeClass("hide");
    jQuery("#video-controls-container").removeClass("hide");
};

let hideVideoContainerOptions = () => {
    jQuery("#video-controls-container").addClass("hide");
    jQuery("#star-icon").addClass("hide");
};

let setVideoMouseOver = () => {
    let videoContainer = jQuery("#video-container");
    let starIcon = jQuery("#star-icon");
    let videoControlsSection = jQuery("#video-controls-container");
    videoContainer.mouseenter(() => {
        showVideoContainerOptions();
    });
    videoContainer.mouseleave(() => {
        hideVideoContainerOptions();
    });
};

let extractName = (memberIdentity) => {
    return memberIdentity.split(":")[0];
};

setVideoMouseOver();
setChatToggle();
