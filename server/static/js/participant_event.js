(($, window) => {
    /* organizer view model start */
    var organizerViewModel = function() {
        var self = this;

        self.chatParticipants = ko.observableArray();
        self.videoParticipants = ko.observableArray();
        self.messages = ko.observableArray();
        self.showChatParticipants = ko.observable(true);
        self.showChatMessages = ko.observable(false);
        self.numParticipants = ko.observable(0);

        self.currentVideoGroup = ko.observableArray();

        self.participantOnclick = function() {
            self.showChatParticipants(true);
            self.showChatMessages(false);
            console.log("showing participants");
        };

        self.chatOnclick = function() {
            self.showChatParticipants(false);
            self.showChatMessages(true);
            scrollChatContainerIntoView();
        };

        self.settingsOnclick = function () {
          console.log("showing user settings");
        };

        self.requestsOnclick = function() {
            console.log("showing requests");
        };

        self.removeMember = function(memberIdentity) {
            self.chatParticipants.remove(function(memberObject) {
               return memberObject.identity == memberIdentity;
            });
        };

        self.getNameFromParticipantIdentity = function(participant) {
            name = participant.identity.split(":")[0];
            return name;
        };

        self.getNameFromMessageAuthor = function(identity) {
            return identity.split(":")[0];
        }
    };

    let viewModelInstance = new organizerViewModel();
    window.viewModel = viewModelInstance;
    ko.applyBindings(viewModelInstance);
    /* organizer view model end */
    var twilio_chat_client = new ChatClient();
    twilio_chat_client.start();
    var twilio_video_client = new VideoClient();
    twilio_video_client.setStartCallOnclick();

})(jQuery, window);
    // let setOrganizerVideoSetting = () => {
    //
    //     let userVideo = $("#video-5");
    //     let videoSetting = $("#video-setting");
    //     videoSetting.click(() => {
    //
    //         if (videoSetting.hasClass("video-off")) {
    //             videoSetting.addClass("video-on");
    //             videoSetting.removeClass("video-off");
    //             videoSetting.css("background", "white");
    //
    //             if (window.room)
    //                 Twilio.Video.createLocalTracks({video: true}).then(tracks => {
    //                     room.localParticipant.publishTracks(tracks).then(tracksPublished => {
    //                         console.log("tracks published");
    //                         console.log(tracksPublished);
    //                         window.tp = tracksPublished;
    //                         tracksPublished.forEach(track => {
    //                             $("#video-5").append(track.track.attach());
    //                         });
    //                     });
    //                 });
    //             else
    //                 twilioRoomOptions.video = true;
    //
    //         } else {
    //             videoSetting.addClass("video-off");
    //             videoSetting.removeClass("video-on");
    //             videoSetting.css("background", "red");
    //
    //             if (window.room) {
    //                 $(document.querySelector("#video-5 video")).remove();
    //                 room.localParticipant.videoTracks.forEach(videoTrack => {
    //                     room.localParticipant.unpublishTrack(videoTrack);
    //                     videoTrack.detach();
    //                     videoTrack.stop();
    //                 });
    //             } else {
    //                 twilioRoomOptions.video = false;
    //             }
    //         }
    //     });
    // };
    //
    // let setOrganizerAudioSetting = () => {
    //     let audioSetting = $("#audio-setting");
    //     audioSetting.click(() => {
    //
    //         if (audioSetting.hasClass("audio-off")) {
    //             audioSetting.addClass("audio-on");
    //             audioSetting.removeClass("audio-off");
    //             audioSetting.css("background", "white");
    //
    //             if (window.room)
    //                 Twilio.Video.createLocalTracks({audio: true}).then(tracks => {
    //                     room.localParticipant.publishTracks(tracks).then(tracksPublished => {
    //                         console.log("tracks published");
    //                         console.log(tracksPublished);
    //                         window.tp = tracksPublished;
    //                         tracksPublished.forEach(track => {
    //                             $("#video-5").append(track.track.attach());
    //                         });
    //                     });
    //                 });
    //             else
    //                 twilioRoomOptions.audio = true;
    //
    //         } else {
    //             audioSetting.addClass("audio-off");
    //             audioSetting.removeClass("audio-on");
    //             audioSetting.css("background", "red");
    //
    //             if (window.room) {
    //                 $(document.querySelector("#video-5 audio")).remove();
    //                 room.localParticipant.audioTracks.forEach(audioTrack => {
    //                     room.localParticipant.unpublishTrack(audioTrack);
    //                     audioTrack.detach();
    //                     audioTrack.stop();
    //                 });
    //             } else {
    //                 twilioRoomOptions.audio = false;
    //             }
    //         }
    //     });
    // };
    //
    // let setParticipantVideoControlSetting = (channel) => {
    //     let participantVideoSetting = $("#participant-video-setting");
    //     participantVideoSetting.click(() => {
    //         if (participantVideoSetting.hasClass("video-off")) {
    //             participantVideoSetting.addClass("video-on");
    //             participantVideoSetting.removeClass("video-off");
    //             participantVideoSetting.css("background", "white");
    //
    //             channel.sendMessage("turn-video-on");
    //
    //         } else {
    //             participantVideoSetting.addClass("video-off");
    //             participantVideoSetting.removeClass("video-on");
    //             participantVideoSetting.css("background", "red");
    //
    //             channel.sendMessage("turn-video-off");
    //         }
    //     });
    // };
    //
    // let setParticipantAudioControlSetting = (channel) => {
    //     let participantAudioSetting = $("#participant-audio-setting");
    //     participantAudioSetting.click(() => {
    //         if (participantAudioSetting.hasClass("audio-off")) {
    //             participantAudioSetting.addClass("audio-on");
    //             participantAudioSetting.removeClass("audio-off");
    //             participantAudioSetting.css("background", "white");
    //
    //             channel.sendMessage("turn-audio-on");
    //
    //         } else {
    //             participantAudioSetting.addClass("audio-off");
    //             participantAudioSetting.removeClass("audio-on");
    //             participantAudioSetting.css("background", "red");
    //
    //             channel.sendMessage("turn-audio-off");
    //         }
    //     });
    // };
    //
    // let setParticipantChatControlSetting = (channel) => {
    //     let participantChatSetting = $("#participant-chat-setting");
    //     participantChatSetting.click(() => {
    //         if (participantChatSetting.hasClass("chat-off")) {
    //             participantChatSetting.addClass("chat-on");
    //             participantChatSetting.removeClass("chat-off");
    //             participantChatSetting.css("background", "white");
    //
    //             channel.sendMessage("turn-chat-on");
    //
    //         } else {
    //             participantChatSetting.addClass("chat-off");
    //             participantChatSetting.removeClass("chat-on");
    //             participantChatSetting.css("background", "red");
    //
    //             channel.sendMessage("turn-chat-off");
    //         }
    //     });
    // };
    //
    // setOrganizerVideoSetting();
    // setOrganizerAudioSetting();



/* video client end */

