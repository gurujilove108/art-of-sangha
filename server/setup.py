import os
from subprocess import call


def install_python_requirements():
    try:
        call(["pip", "install", "-t", "lib", "-r", "requirements.txt"])
    except Exception as e:
        print "[+] Encountered exception while issuing command pip install -t lib -r requirements.txt"
        print str(e)


def install_npm_requirements():
    try:
        call(["npm", "install"])
    except Exception as e:
        print "[+] Encountered exception while issuing command npm install"
        print str(e)


install_python_requirements()
# install_npm_requirements()
