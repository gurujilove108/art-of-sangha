import time
from flask import Flask, redirect, render_template, request, jsonify, url_for
from helper_functions import create_event_organizer_link, is_organizer
from middleware import MiddleWare
from ndb_models.event import Event
from twilio_handler import *
from gaesessions import get_current_session
from google.appengine.api import users
from google.appengine.ext import ndb


from blueprints.event_blueprint import event_blueprint
from blueprints.index_blueprint import index_blueprint
from blueprints.test_blueprint import test_blueprint
from blueprints.twilio_blueprint import twilio_blueprint
import jinja2
import os
import json


# flask app and jinja2 settings
def create_flask_app():
    app = Flask(__name__)
    app.wsgi_app = MiddleWare(app.wsgi_app)
    template_dir = os.path.join(os.path.dirname(__file__), 'templates')
    jinja_env = jinja2.Environment(loader=jinja2.FileSystemLoader(template_dir), autoescape=True)
    app.jinja_env = jinja_env
    app.debug = True
    return app


app = create_flask_app()
app.register_blueprint(event_blueprint)
app.register_blueprint(index_blueprint)
app.register_blueprint(test_blueprint)
app.register_blueprint(twilio_blueprint)



