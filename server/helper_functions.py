import logging
import os
import json
import time
from google.appengine.api import users

production_url = "https://art-of-sangha.appspot.com"
development_url = "http://localhost:3000"


# log a msg so we dont call logging.info every time
def log(msg):
    logging.info(msg)


# when developing a google app engine application
# the environment variable 'SERVER SOFTWARE' will be Development/x.y
# x.y being the version number
def is_development():
    return os.environ.get("SERVER_SOFTWARE").startswith("Development")


# when an event is created and then stored
# a unique url path will be created for that event
# based on whether the app is running is production, or development the link will be different so act accordingly
def create_event_organizer_link(event_url_path):
    if is_development():
        return "%s/%s" % (development_url, event_url_path)
    return "%s/%s" % (production_url, event_url_path)


def is_organizer(event_organizers_json, google_user):
    if not google_user:
        return False, None
    organizers = json.loads(event_organizers_json)
    for organizer in organizers:
        if organizer.get("email") == google_user.email():
            return True, capitalize_name(organizer.get("name"))
    return False, None


# the arguments passed to the join_event view
def create_join_event_client_args(event, event_link):
    return {
        "event": event.to_dict(),
        "organizer_login_url": users.create_login_url("/%s/organizer" % event_link),
        "participant_login_url": event_link
    }


# the arguments passed to the organizer_event view
def create_organizer_event_client_args(event, user, organizer_name):
    return {
        "event": event.to_dict(),
        "name": organizer_name,
        "is_organizer": True,
        "current_time": time.time()
    }


def capitalize_name(name):
    return name.title()


# arguments passed to participant_event
def create_participant_event_args(event, name, organizers):
    return {
        "event": event.to_dict(),
        "current_time": time.time(),
        "name": name,
        "organizers": ", ".join(organizers)
    }


