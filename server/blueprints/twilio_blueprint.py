from flask import Blueprint, jsonify, request, render_template, redirect
from gaesessions import get_current_session
from helper_functions import *
from ndb_models.event import Event
from twilio_handler import *
twilio_blueprint = Blueprint('twilio_blueprint', __name__, template_folder='templates')


@twilio_blueprint.route('/delete_channels')
def delete_channels():
    try:
        delete_twilio_channels()
    except Exception as e:
        log(str(e))


# access tokens expire, therefore we have built the following tool. Cheers
@twilio_blueprint.route('/chat_access_token')
def chat_access_token():
    try:
        session = get_current_session()
        if session.get("is_participant"):
            token = create_chat_token(session.get("participant_email"), session.get("participant_username"))
            return jsonify({"token": token})

        elif session.get("is_organizer"):
            token = create_chat_token(users.get_current_user().email(), session.get("organizer_name"))
            return jsonify({"token": token})

        else:
            raise Exception("user is not a participant or an organizer")

    except Exception as e:
        return jsonify({"error": True, "exception": str(e)})


# for video access tokens we require the event link because the event link is the unique identifier for the room
@twilio_blueprint.route('/<event_link>/video_access_token', methods=["get"])
def video_access_token(event_link):
    try:
        session = get_current_session()
        if session.get("is_participant"):
            token = create_video_token(session.get("participant_email"), event_link)
            return jsonify({"token": token})

        elif session.get("is_organizer"):
            token = create_video_token(users.get_current_user().email(), event_link)
            return jsonify({"token": token})

        else:
            raise Exception("user is not a participant or an organizer")
    except Exception as e:
        return jsonify({"error": True, "exception": str(e)})


# twilio webhooks
@twilio_blueprint.route('/webhooks/twilio_chat/pre_event', methods=["get", "post"])
def twilio_chat_webhook_prevent():
    log("Twilio chat pre event")
    log(request.form)
    return ""


@twilio_blueprint.route('/webhooks/twilio_chat/post_event', methods=["get", "post"])
def twilio_chat_webhook_postevent():
    log("Twilio chat post event")
    log(request.form)
    return ""


@twilio_blueprint.route('/webhooks/twilio_video')
def twilio_video_webhook():
    log(request)
    return ""


@twilio_blueprint.route('/twilio_rooms')
def twilio_rooms():
    try:
        pass
    except Exception as e:
        log(str(e))



