from flask import Blueprint, render_template, redirect, url_for, jsonify
from gaesessions import get_current_session
from google.appengine.ext import ndb
from ndb_models.event import Event
from helper_functions import *
from twilio_handler import *
test_blueprint = Blueprint('test_blueprint', __name__, template_folder='templates')


@test_blueprint.route('/test')
def test():
    return render_template("test.html")


@test_blueprint.route('/delete_events')
def delete_events():
    ndb.delete_multi([entity.key for entity in Event.query()])


@test_blueprint.route("/clear_session")
def clear_session():
    try:
        get_current_session().clear()
        return redirect(url_for("test"))
    except Exception as e:
        log(str(e))


@test_blueprint.route('/event_json')
def event_json():
    events = []
    for event in Event.query().fetch():
        event.organizer_link = create_event_organizer_link(event.organizer_link)
        events.append(event.to_dict())
    return jsonify(events)


# testing routes
@test_blueprint.route('/create_random_event')
def create_random_event():
    vinods_data = json.loads('{"center":{"name":"Art of Living Los Angeles","phone":"","email":"losangeles@us.artofliving.org","address":""},"location":{"latitude":"0.000000","longitude":"0.000000"},"address":{"country":"","state":"","zipcode":"","city":"","street_address_1":"","street_address_2":""},"event_end":{"timezone":"America/Los_Angeles","utc":"2018-02-28T10:00:00","local":"2018-02-28T02:00:00"},"event_start":{"timezone":"America/Los_Angeles","utc":"2018-02-28T09:00:00","local":"2018-02-28T01:00:00"},"template_id":"5a38bd92cb3860207264d2b8","additional_details":"","online_session_url":"www.sdf.mnb","speaker_name":"john","event_capacity":"23","event_description":"","event_series_name":"00609ba1a3d266bc4cf6502efccecf8e","event_name":"Mind and Meditation","event_id":"a163A000009qUR9QAM","organizers":[{"phone":"6178589140","email":"dylans.codes@gmail.com","name":"Dylan Dannenhauer"}],"private_event":false,"event_type":"online","event_status":"active"}')
    vinods_data["event_id"] = create_random_link()
    Event.store_event(vinods_data)
    return redirect("/test")


