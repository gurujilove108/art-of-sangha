from flask import Blueprint, jsonify, request, render_template, redirect, url_for
from gaesessions import get_current_session

from helper_functions import *
from ndb_models.event import Event
from twilio_handler import *

event_blueprint = Blueprint('event_blueprint', __name__, template_folder='templates')


@event_blueprint.route('/create_online_event', methods=["post"])
def create_online_event():
    try:
        online_event_dict = request.get_json(force=True)
        if Event.event_exists(online_event_dict["event_id"]):
            return jsonify({"error": True, "success": False, "message": "An event with that id has already been created. Event id's must be unique"})
        event_organizer_url_path = Event.store_event(online_event_dict)
        event_organizer_url = create_event_organizer_link(event_organizer_url_path)
        return jsonify({"organizer_link": event_organizer_url, "success": True})
    except Exception as e:
        log(str(e))
        return jsonify({"error": True, "success": False, "message": "server error"})


@event_blueprint.route('/<event_link>')
def join_event(event_link):
    try:
        event = Event.by_link(event_link)
        if not event:
            return render_template("bad_event_link.html")
        elif event.expired:
            return render_template("event_expired.html")
        else:
            join_event_args = create_join_event_client_args(event, event_link)
            return render_template(
                "join_event.html",
                **join_event_args
            )
    except Exception as e:
        log(str(e))


@event_blueprint.route('/<event_link>/organizer')
def render_organizer_event(event_link):
    try:
        event = Event.by_link(event_link)
        user = users.get_current_user()
        if not user:
            return redirect(url_for("event_blueprint.join_event", event_link=event_link))
        organizer_true, organizer_name = is_organizer(event.organizers, user)
        if user and organizer_true:
            session = get_current_session()
            session["is_organizer"] = True
            session["organizer_name"] = organizer_name
            organizer_event_args = create_organizer_event_client_args(event, user, organizer_name)
            return render_template(
                "organizer_event.html",
                **organizer_event_args
            )
        else:
            join_event_args = create_join_event_client_args(event, event_link)
            return render_template(
                "join_event.html",
                organizer_login_error=True,
                **join_event_args
            )
    except Exception as e:
        log(str(e))


@event_blueprint.route('/<event_link>/participant', methods=["post", "get"])
def render_participant_event(event_link):
    try:
        event = Event.by_link(event_link)
        session = get_current_session()

        # if the user goes to this page and has already received the share page then just go straight to the event
        if session.get("is_participant"):
            organizers = event.get_organizers()
            participant_event_args = create_participant_event_args(event, session.get("participant_username"), organizers)
            return render_template(
                "participant_event.html",
                **participant_event_args
            )

        # otherwise participant is joining this page for first time so grab the form data, add to db, and display
        # the participant_share page
        username = request.form["participant_username"].title()
        log(username)
        email = request.form["participant_email"].strip()
        phone = request.form["participant_phone"].strip()

        if not username or not email or not phone:
            join_event_client_args = create_join_event_client_args(event, event_link)
            return render_template(
                "join_event.html",
                form_error=True,
                **join_event_client_args
            )

        event.add_participant(username, email, phone)
        session["is_participant"] = True
        session["participant_username"] = username
        session["participant_email"] = email

        organizers = event.get_organizers()
        participant_event_args = create_participant_event_args(event, session.get("participant_username"), organizers)
        return render_template(
            "participant_event.html",
            **participant_event_args
        )
    except Exception as e:
        log(str(e))
        return redirect(url_for("event_blueprint.join_event", event_link=event_link))


@event_blueprint.route('/<event_link>/<user_type>/get_or_create_channel')
def create_twilio_chat_channel(event_link, user_type):
    try:
        user = users.get_current_user()
        session = get_current_session()
        event = Event.by_link(event_link)
        if not session.get("is_organizer"):
            raise Exception("user not logged in when creating twilio channel")

        if event.channel is None:
            sid, unique_name = create_channel(event.title)
            event.create_twilio_channel(unique_name, sid)
            add_member_to_channel(user.email(), session.get("organizer_name"), sid)
            return jsonify({"sid": sid})

        else:
            sid = event.channel.sid
            try:
                add_member_to_channel(user.email(), session.get("organizer_name"), sid)
                return jsonify({"sid": sid})

            except Exception as e:
                return jsonify({"sid": sid})

    except Exception as e:
        return jsonify({"error": True, "exception": str(e)})


@event_blueprint.route('/<event_link>/participant/channel_sid')
def channel_sid(event_link):
    try:
        session = get_current_session()
        event = Event.by_link(event_link)
        if event.channel is None:
            return jsonify({"wait": True})
        else:
            sid = event.channel.sid
            try:
                log(session.get("participant_email"))
                log(session.get("participant_username"))
                add_member_to_channel(session.get("participant_email"), session.get("participant_username"), sid)
            except Exception as e:
                return jsonify({"sid": sid})

    except Exception as e:
        return jsonify({"error": True, "exception": str(e)})


@event_blueprint.route('/<event_link>/organizer/end_session')
def organizer_logout(event_link):
    try:
        user = users.get_current_user()
        event = Event.by_link(event_link)
        if user and is_organizer(event.organizers, user):
            event.set_expired(True)
            return jsonify({"expired": True})
        else:
            return jsonify({"access": "denied"})

    except Exception as e:
        log(str(e))


@event_blueprint.route('/<event_link>/organizer/start_video')
def start_video_conference(event_link):
    try:
        session = get_current_session()
        # create_twilio_room(event_link)
        video_access_token = create_video_token(users.get_current_user().email(), session.get("organizer_name"), event_link)
        return jsonify({"token": video_access_token})
    except Exception as e:
        return jsonify({"exception": str(e)})


@event_blueprint.route('/<event_link>/participant/start_video')
def participant_start_video(event_link):
    try:
        session = get_current_session()
        video_access_token = create_video_token(session.get("participant_email"), session.get("participant_username"), event_link)
        return jsonify({"token": video_access_token})
    except Exception as e:
        return jsonify({"exception": str(e)})


@event_blueprint.route('/<event_link>/participant/check_if_organizer', methods=["post"])
def check_if_organizer(event_link):
    try:
        event = Event.by_link(event_link)
        possible_host = request.form["participant_identity"]
        if event.has_organizer(possible_host):
            return jsonify({"is_organizer": True})
        else:
            return jsonify({"is_organizer": False})
    except Exception as e:
        return jsonify({"exception": str(e)})







