from twilio.rest import Client
from twilio.jwt.access_token import AccessToken
from twilio.jwt.access_token.grants import ChatGrant, VideoGrant
from helper_functions import *
from ndb_models.twilio_channel import TwilioChannel
from ndb_models.database_helper import create_random_link
import json

# twilio configuration, and yes this will be in a safe config file in phase 2
twilio_live_sid = "AC185fa0a0119ee112bf06904f6e592c20"
twilio_live_token = "c3df501aae2cc76ee62b0390d98822b2"
twilio_test_sid = "AC839455f1b2db5553736f51391cd21b7a"
twilio_test_token = "1cf2319b42c99c337db603c574badc38"
twilio_apikey_sid = "SK85461998109efd22dbcd67afd26eac59"
twilio_apikey_secret = "EvEVQCTxHNfqDlfDJsW59nphr98Y12Q8"

dylans_chat_service = "IS79ceea52c2af4b148c996dd835c4c57e"
twilio_client = Client(twilio_live_sid, twilio_live_token)


# create access token for twilio chat
def create_chat_token(email, name):
    chat_token_identity = name + ":" + email
    chat_token = AccessToken(
        twilio_live_sid,
        twilio_apikey_sid,
        twilio_apikey_secret,
        identity=chat_token_identity
    )

    chat_grant = ChatGrant(service_sid=dylans_chat_service)
    chat_token.add_grant(chat_grant)
    return chat_token.to_jwt()


# create access token for twilio video
def create_video_token(email, name, event_link):
    video_token_identity = name + ":" + email
    video_token = AccessToken(
        twilio_live_sid,
        twilio_apikey_sid,
        twilio_apikey_secret,
        identity=video_token_identity
    )

    video_grant = VideoGrant(room=event_link)
    video_token.add_grant(video_grant)
    return video_token.to_jwt()


# def create_twilio_room(event_link):
#     try:
#         room_that_might_exist = twilio_client.video.rooms.list(unique_name=event_link)
#         log("logging room that might exist")
#         log(room_that_might_exist)
#         if not room_that_might_exist:
#             new_room = twilio_client.video.rooms.create(
#                 unique_name=event_link,
#                 type="group",
#                 record_participants_on_connect=False,
#             )
#     except Exception as e:
#         "exception raised when creaing twilio video room"
#         log(str(e))


# list all channels
def get_channels():
    channels = twilio_client.chat.services(dylans_chat_service).channels.list()
    return channels


def get_channel_unique_names():
    unique_names = []
    channels = get_channels()
    for channel in channels:
        unique_names.append(channel.unique_name)
    return unique_names


def create_unique_twilio_channel_name():
    unique_names = get_channel_unique_names()
    new_channel_unique_name = create_random_link()
    while new_channel_unique_name in unique_names:
        new_channel_unique_name = create_random_link()
    return new_channel_unique_name


# create a channel
def create_channel(event_title):
    try:
        unique_name = create_unique_twilio_channel_name()
        channels = twilio_client.chat.services(dylans_chat_service).channels

        new_channel = channels.create(
            friendly_name=event_title,
            unique_name=unique_name,
            type="private")

        sid = new_channel.sid
        return sid, unique_name
    except Exception as e:
        log(str(e))


# delete channel
def delete_twilio_channels():
    channels = get_channels()
    for channel in channels:
        channel.delete()


# for twilio private channels, every member has to be specifically added to the channel
# this will happen when an organizer creates a channel and when a participant is added to a channel
def add_member_to_channel(email, name, sid):
    identity = name + ":" + email
    member = twilio_client.chat \
        .services(dylans_chat_service) \
        .channels(sid) \
        .members \
        .create(identity)
    log("member %s has been added to channel %s" % (email, sid))
    log(member)




